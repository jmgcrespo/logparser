module Collector
  class Base
    def add entry
      self.process entry
    end

    def entries
      @entries ||= Hash.new(0)
    end

    def ordered_entries
      entries.sort_by { |key, value| value }.reverse
    end

    def top n = 10
      ordered_entries[0...n]
    end
  end
end
