require_relative 'base'
module Collector
  class UniqueVisits < Base
    class Entry
      attr_accessor :visits

      def initialize
        @visits = 0
      end

      def add ip
        unless clients.include?(ip)
          @visits += 1
          clients[ip] = 0
        end
      end

      def clients
        @clients ||= {}
      end
    end

    def entries
      @entries ||= Hash.new { |h, k| h[k] = Entry.new }
    end

    def ordered_entries
      entries.sort_by { |k, v| v.visits }.map { |e| [e[0], e[1].visits]}.reverse
    end

    def process entry
      entries[entry[:path]].add entry[:ctl_ip]
    end

    Parser.register(:unique_visits, self)
  end
end

