require_relative 'base'
module Collector
  class Visits < Base

    def process entry
      entries[entry[:path]] += 1
    end

    Parser.register( :visits, self)
  end
end

