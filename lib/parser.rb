class Parser
  LOG_FORMAT = /\A(?<path>.*)\s(?<ctl_ip>\d+\.\d+\.\d+\.\d+)\Z/
  class LogFormatError < StandardError;end

  attr_reader :log, :enabled_collectors
  class << self
    def register name, collector
      registered_collectors[name] = collector
    end

    def registered_collectors
      @registered_collectors ||= {}
    end
  end

  def initialize log, *collectors
    @log = log
    initialize_collectors collectors
  end

  def parse
    log.each_line do |line|
      raise LogFormatError unless entry = LOG_FORMAT.match(line)
      add_entry_to_collectors entry
    end
    results
  end

  def add_entry_to_collectors entry
    enabled_collectors.each_value do |collector|
      collector.add entry
    end
  end

  def results
    Struct.new("Results", *enabled_collectors.keys).new(*enabled_collectors.values)
  end

  private

  def initialize_collectors collectors
    collectors.each do |name|
      raise ArgumentError, "Unknown collector #{name}" unless  collector = registered_collectors[name]

      enabled_collectors[name] = collector.new
    end
  end

  def enabled_collectors
    @enabled_collectors ||= {}
  end

  def registered_collectors
    Parser.registered_collectors
  end
end
