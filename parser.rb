#!/usr/bin/env ruby
require_relative 'lib/parser'
require_relative 'lib/collector/visits'
require_relative 'lib/collector/unique_visits'

filename = ARGV.pop.chomp

begin
  file = File.open(filename, 'r')
rescue StandardError => e
  puts "Error while opening file: #{ e.message }"
  exit 1
end

parser = Parser.new(file, :visits, :unique_visits)
results = parser.parse

puts 'Most Visited'
results.visits.ordered_entries.each { |e| puts e.join(' ') }
puts 
puts 'Most Visited'
results.unique_visits.ordered_entries.each { |e| puts e.join(' ') }
