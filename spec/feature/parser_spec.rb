RSpec.describe "parser.rb" do
  let(:log) { 'spec/fixtures/test_file.log' }
  it 'requires a file' do
    run = `./parser.rb no_exists.log 2>&1`
    expect(run).to match "Error"
    expect($?.exitstatus).to eq 1

    run = `./parser.rb #{ log } 2>&1`
    expect($?.exitstatus).to eq 0
  end
end
