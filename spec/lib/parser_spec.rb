RSpec.describe Parser do
  let(:test_file) { File.open('spec/fixtures/test_file.log') }
  let(:collector) { class_double('TestCollector') }

  describe 'enabled_collectors' do
    it 'has visits collector' do
      expect(Parser.registered_collectors).to include(visits: Collector::Visits)
    end
    it 'has visits collector' do
      expect(Parser.registered_collectors).to include(unique_visits: Collector::UniqueVisits)
    end
  end

  describe 'registering collectors' do

    it 'adds collector to enabled collector list' do
      Parser.register(:test, collector)
      expect(Parser.registered_collectors).to include(test: collector)
    end
  end

  describe "#parse" do
    it 'adds line entries to collectors' do
      visits = double
      allow(visits).to receive(:add)
      allow(Collector::Visits).to receive(:new).and_return(visits)

      Parser.register(:test, collector)
      test_collector_instance = double
      allow(test_collector_instance).to receive(:add)
      allow(collector).to receive(:new).and_return(test_collector_instance)


      parser = Parser.new(test_file, :test, :visits)
      parser.parse
      expect(visits).to have_received(:add).exactly(500)
      expect(test_collector_instance).to have_received(:add).exactly(500)
    end

    it 'returns results with collectors' do
      parser = Parser.new(test_file, :visits, :unique_visits)
      results = parser.parse
      visits = results.visits.ordered_entries
      expect(visits.first).to eq(['/about/2', 90])
      expect(visits.last).to eq(['/home', 78])

      unique_visits = results.unique_visits.ordered_entries
      expect(unique_visits.first).to eq(['/index', 23])
      expect(unique_visits.last).to eq(['/about', 21])
    end
  end
end
